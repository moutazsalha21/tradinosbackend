<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubTask extends Model
{
    protected $fillable = ['title'];

    public function task()
    {
        return $this->belongsTo(Task::class);
    }
}
