<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                "email" => "required|email",
                "password" => "required",
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 401, "validation_errors" => $validator->errors()]);
            }

            $credentials = [
                'email' => $request->email,
                'password' => $request->password
            ];

            if (auth()->attempt($credentials)) {
                $user = Auth::user();
                $token = $user->createToken('AppName')->accessToken;
                $role = $user->getRoleNames();
                return response()->json(['status' => 200, 'token' => $token, 'role' => $role, 'user' => $user]);
            } else {
                return response()->json(['status' => 401, 'error' => 'Unauthorised']);
            }
        } catch (Exception $error) {
            return response()->json([
                "status_code" => 500,
                "message" => "Error in login",
                "error" => $error
            ]);
        }
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 401, 'error' => $validator->errors()]);
        }

        $data = $request->all();

        $data['password'] = Hash::make($data['password']);

        $user = User::create($data);

        $user->assignRole('User');

        return response()->json(['status' => 200, "message" => "Success! registration completed", "user" => $user]);
    }
}
