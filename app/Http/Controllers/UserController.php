<?php

namespace App\Http\Controllers;

use App\User;

class UserController extends Controller
{
    public function getAllUser()
    {
        $users = User::all();
        return response()->json([
            'status' => 200,
            'users' => $users
        ]);
    }
}
