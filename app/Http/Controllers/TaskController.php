<?php

namespace App\Http\Controllers;

use App\Task;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function index()
    {
        $authUser = auth()->user();
        if ($authUser->hasAnyRole('Admin', 'User')) {
            $tasks = Task::with(['user', 'categories'])->get();
            return response()->json([
                'status' => 200,
                'tasks' => $tasks,
            ]);
        } else {
            abort(404);
        }
    }

    public function store(Request $request)
    {
        try {
            $task = new Task();
            $task->description = $request->description;
            $task->deadLine = $request->deadLine;
            $task->endFlag = $request->endFlag;
            $task->user_id = $request->userAssign;
            $task->save();

            foreach ($request->subTask as $subTask) {
                $task->subTasks()->create([
                    'title' => $subTask['sub']
                ]);
            }

            foreach ($request->categoryAssign as $category) {
                $task->categories()->attach([$category['id']]);
            }
            return response()->json([
                'status' => 200,
                'msg' => 'Success! create task completed',
                'task' => $task,
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'error' => $exception->getMessage(),
            ]);
        }
    }

    public function show($id)
    {
        $authUser = auth()->user();
        if ($authUser->hasAnyRole('Admin', 'User')) {

            $task = Task::with(['subTasks', 'user', 'categories'])->where('id', $id)->get();
            if ($task->isEmpty()) {
                abort(404);
            } else {
                return response()->json([
                    'status' => 200,
                    'task' => $task
                ]);
            }
        } else {
            return response()->json([
                'status' => 401,
                'msg' => 'Unauthorized'
            ]);
        }
    }

    public function destroy($id)
    {
        try {
            $task = Task::findOrFail($id);
            if (auth()->user()->can('delete', $task)) {
                $task->delete();
                return response()->json([
                    'status' => 200,
                    'msg' => 'Success! deleted completed'
                ]);
            } else {
                return response()->json([
                    'status' => 401,
                    'msg' => 'Unauthorized'
                ]);
            }
        } catch (ModelNotFoundException $exception) {
            return response()->json([
                'error' => $exception->getMessage()
            ]);
        }
    }

    public function search(Request $request)
    {
        $endFlag = $request->endFlag;
        $deadLine = $request->deadLine;

        if (!is_null($endFlag) && !is_null($deadLine)) {
            $tasks = Task::whereDate('deadLine', $deadLine)
                ->where('endFlag', $endFlag)
                ->with(['user', 'categories'])
                ->get();
        } elseif (!is_null($endFlag) && is_null($deadLine)) {
            $tasks = Task::where('endFlag', $endFlag)
                ->with(['user', 'categories'])
                ->get();
        } elseif (is_null($endFlag) && !is_null($deadLine)) {
            $tasks = Task::whereDate('deadLine', $deadLine)
                ->with(['user', 'categories'])
                ->get();
        }

        return response()->json([
            'status' => 200,
            'tasks' => $tasks,
        ]);
    }

    // for test
    public function schedule()
    {
        $tasks = Task::where('endFlag', 0)->get();
        if (!$tasks->isEmpty()) {
            foreach ($tasks as $task) {
                if (Carbon::parse($task->deadLine)->lt(Carbon::now())) {
                    $task->endFlag = 1;
                    $task->save();
                }
            }
        }
        return response()->json([
            'tasks' => $tasks
        ]);
    }
}
