<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function tasks()
    {
        return $this->belongsToMany(Task::class, 'task_category', 'categories_id', 'task_id');
    }
}
