<?php

namespace App\Console;

use App\Jobs\SendEmailJob;
use App\Task;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();

        $schedule->call(function () {
            $tasks = Task::where('endFlag', 0)->get();
            if (!$tasks->isEmpty()) {
                foreach ($tasks as $task) {
                    if (Carbon::parse($task->deadLine)->lt(Carbon::now())) {
                        $task->endFlag = 1;
                        $task->save();
                        $details['email'] = $task->user->email;
                        dispatch(new SendEmailJob($details));
                    }
                }
            }
        })->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
