<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function subTasks()
    {
        return $this->hasMany(SubTask::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'task_category', 'task_id', 'categories_id');
    }
}
