<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(Role_Permission::class);
        $this->call(TaskSeeder::class);
        $this->call(SubTaskSeeder::class);
        $this->call(CategorySeeder::class);
    }
}
