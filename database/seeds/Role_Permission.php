<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class Role_Permission extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions for user model
        Permission::create(['name' => 'create task']);
        Permission::create(['name' => 'view all task']);
        Permission::create(['name' => 'view task by id']);
        Permission::create(['name' => 'delete task']);

        Permission::create(['name' => 'create subTask']);
        Permission::create(['name' => 'view subTask']);
        Permission::create(['name' => 'delete subTask']);

        Permission::create(['name' => 'create category']);
        Permission::create(['name' => 'view category']);
        Permission::create(['name' => 'delete category']);

        // create roles and assign existing permissions
        $role1 = Role::create(['name' => 'Admin']);
        $role1->syncPermissions([
            'create task', 'view all task', 'view task by id', 'delete task',
            'create subTask', 'view subTask', 'delete subTask',
            'create category', 'view category', 'delete category'
        ]);

        $role2 = Role::create(['name' => 'User']);
        $role2->syncPermissions([
            'view all task', 'view task by id',
            'view subTask',
            'view category'
        ]);

        // create users and assign existing roles
        $adminUser = User::create([
            'name' => 'Admin',
            'email' => 'admin@yahoo.com',
            'password' => Hash::Make(123456),
        ]);
        $adminUser->assignRole($role1);

        $clientUser_1 = User::create([
            'name' => 'user1',
            'email' => 'user1@yahoo.com',
            'password' => Hash::Make(123456),
        ]);
        $clientUser_1->assignRole($role2);

        $clientUser_2 = User::create([
            'name' => 'user2',
            'email' => 'user2@yahoo.com',
            'password' => Hash::Make(123456),
        ]);
        $clientUser_2->assignRole($role2);
    }
}
