<?php

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::find(2);
        $user->tasks()->create([
            'description' => 'Build the front-end web application for tradinos test',
            'deadLine' => Carbon::parse('2021-10-26'),
        ]);

        $user->tasks()->create([
            'description' => 'Build the back-end web application for tradinos test',
            'deadLine' => Carbon::parse('2021-10-26'),
        ]);

        $user->tasks()->create([
            'description' => 'Build the dataBase schema for tradinos test',
            'deadLine' => Carbon::parse('2021-10-26'),
        ]);

        $user = User::find(3);
        $user->tasks()->create([
            'description' => 'Build the machine learning model for text analysis',
            'deadLine' => Carbon::parse('2021-10-24'),
        ]);

        $user->tasks()->create([
            'description' => 'Building a web application to encapsulate a text analysis form',
            'deadLine' => Carbon::parse('2021-10-24'),
        ]);
    }
}
