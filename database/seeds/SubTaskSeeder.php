<?php

use App\Task;
use Illuminate\Database\Seeder;

class SubTaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $task_1 = Task::find(1);

        $task_1->subTasks()->create([
            'title' => 'Build Notification System'
        ]);

        $task_1->subTasks()->create([
            'title' => 'Build Authentication System'
        ]);

        $task_3 = Task::find(3);

        $task_3->subTasks()->create([
            'title' => 'Draw ERD For DataBase Schema'
        ]);
    }
}
