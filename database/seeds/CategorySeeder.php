<?php

use App\Category;
use App\Task;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'name' => 'Very Important',
            'color' => 'Red'
        ]);

        Category::create([
            'name' => 'Medium Importance',
            'color' => 'Blue'
        ]);

        Category::create([
            'name' => 'Not Important',
            'color' => 'Green'
        ]);

        $task_1 = Task::find(1);
        $task_1->categories()->attach([1, 2, 3]);

        $task_2 = Task::find(2);
        $task_2->categories()->attach([1]);

        $task_3 = Task::find(3);
        $task_3->categories()->attach([2]);

        $task_4 = Task::find(4);
        $task_4->categories()->attach([3]);

        $task_5 = Task::find(5);
        $task_5->categories()->attach([2, 3]);
    }
}
